$(document).ready(() => {
    const ACTIVE = "active",
            BODY = $('body'),
            BODY__Classes = document.body.className,
            ViewWidth = window.innerWidth;

    class CommonElements{

        constructor(){

            svg4everybody();
            setTimeout(function(){
              $('.loading').fadeOut(500, function () {
                $('.loading').remove();
              });
            }, 2000);

            setTimeout(function(){
              $("[data-anim]").addClass('anim');
            }, 3000);

            //========================================

            AOS.init({
              offset: 150,
              duration: 600
            });


            $('.button-toggle').click(function(){
              BODY.toggleClass('menu-on');
            });

        }
    }


    class MainPage {

      static handleUp(){

        var element = document.querySelector('.header');
        var scrollUp =  document.getElementById('scrollUp');

        var offset = element.clientHeight + 300;

        window.onscroll = function(){
          window.pageYOffset < offset ? 
            $(scrollUp).removeClass('fixed') : $(scrollUp).addClass('fixed');
        }

        scrollUp.onclick = function(){
          $('html,body').animate({scrollTop: 0}, 500);
        }
      }

      static handleCommon(){
        $('.menu a[href]').click(function(e) {
          let href = this.getAttribute('href');
          let $item = $(this).parent();

          $('.menu li').removeClass('active');
          $($item).addClass('active');

          if(href.indexOf('#') == 0){
              var $target = '.' + href.replace(/^\#/,'');
              var targetOffset = $($target).offset().top;

              $('html,body').animate({scrollTop: targetOffset}, 500);
              return false;
          }else{
              return true;
          }
        });
      }

      static handleVideo(){
        $('[data-fancybox]').fancybox({
          buttons   : false,
          smallBtn  : true,
          afterLoad : function( instance, slide ) {
            if ( slide.type === 'iframe' ) {
              $('<a class="fancybox-close" data-fancybox-close></a>').appendTo( slide.$content );
            }
          }
        });
      }

      static handleCheck(){

        $('[data-mgs]').click(function(e){
          //e.preventDefault();
          let self_pic = document.getElementById("mgs-pic");
          let self_bg = document.getElementById("mgs-bg"); 
          let pic = this.getAttribute("data-mgs");
          let bg = this.getAttribute("data-mgs-bg");

          let imageP = new Image();
              imageP.src = pic;
              imageP.className = 'on';

          let imageB = new Image();
              imageB.src = bg;
              imageB.className = 'on';

          let tmp = $(self_pic)
                      .parent()
                      .find("img");

              tmp.addClass('off');

              self_pic.appendChild(imageP);
              self_bg.appendChild(imageB);

              setInterval(() => {
                tmp.remove();
              }, 600);
        });

        $('[data-pw]').click(function(e){
          //e.preventDefault();
          let self_pic = document.getElementById("pw-pic");
          let self_bg = document.getElementById("pw-bg"); 
          let pic = this.getAttribute("data-pw");
          let bg = this.getAttribute("data-pw-bg");

          let imageP = new Image();
              imageP.src = pic;
              imageP.className = 'on';

          let imageB = new Image();
              imageB.src = bg;
              imageB.className = 'on';

          let tmp = $(self_pic)
                      .parent()
                      .find("img");

              tmp.addClass('off');

              self_pic.appendChild(imageP);
              self_bg.appendChild(imageB);

              setInterval(() => {
                tmp.remove();
              }, 600);
        });

      }

      static Accordion(){
        $('.faq-item__title').click(function () {
            $(this).parents('.faq-item').toggleClass(ACTIVE);
            $(this).next().slideToggle(400);
        });
      }
    }
    

    if(BODY.hasClass('page-index')){
      MainPage.handleCheck();
      MainPage.handleVideo();
      MainPage.handleCommon();
      MainPage.handleUp();
    }
    if(BODY.hasClass('page-faq')){
      MainPage.Accordion();
      MainPage.handleCommon();
      MainPage.handleUp();
    }

    new CommonElements();
});
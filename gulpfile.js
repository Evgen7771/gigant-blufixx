var gulp = require('gulp'),
    watch = require('gulp-watch'),
    pug = require('gulp-pug'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    cssnano = require('gulp-cssnano'),
    rigger = require('gulp-rigger'),
    spritesvg = require('gulp-svg-sprite'),
    rimraf = require('rimraf'),
    gulpBabel = require('gulp-babel'),
    ifPlugin = require('gulp-if');

// Конфиг

var config = {
  production: false
};

var path = {
  build: {
    html: 'build/',
    js: 'build/js/',
    css: 'build/css/',
    img: 'build/img/',
    fonts: 'build/fonts/',
    root: 'build/'
  },
  src: {
    html: './src/pug/*.pug',
    js: './src/js/main.js',
    jsvendor: './src/js/vendor.js',
    sass: './src/sass/main.sass',
    sassvendor: './src/sass/vendor.sass',
    img: ['./src/img/**/*.*', './src/img/**/*.svg', '!./src/img/sprite/**/*.*'],
    spritesvg: './src/img/sprite/svg/*.svg',
    fonts: './src/fonts/**/*.*',
    spriteImg: './src/img/',
    root: ['./src/*', './src/.*', './src/*.*', '!./src/{pug, pug/**}', '!./src/{doc, doc/**}', '!./src/{sass, sass/**}', '!./src/tmp']
  },
  watch: {
    html: './src/pug/**/*.*',
    js: './src/js/custom/*.js',
    jsvendor: './src/js/vendor/*.js',
    sass: './src/sass/custom/**/*.sass',
    sassvendor: './src/sass/vendor/**/*.sass',
    img: './src/img/**/*.*',
    fonts: './src/fonts/**/*.*',
    root: './src/*'
  },
  clean: 'build/'
};


//HTML build===============================================>
gulp.task('html:build', function () {
  gulp.src(path.src.html)
      .pipe(pug({
          pretty: true
      }))
      .pipe(gulp.dest(path.build.html));
});

//SASS build==============================================>
gulp.task('sass:build', function () {
  gulp.src(path.src.sass)
      .pipe(sass())
      .pipe(autoprefixer())
      .pipe(cssnano({
        zindex: false
      }))
      .pipe(gulp.dest(path.build.css));
});

//SASS vendor build=======================================>
gulp.task('sassvendor:build', function () {
  gulp.src(path.src.sassvendor)
      .pipe(sass())
      .pipe(cssnano({
        zindex: false
      }))
      .pipe(gulp.dest(path.build.css));
});

//Main JS build==========================================> 
gulp.task('js:build', function () {
  gulp.src(path.src.js)
      .pipe(rigger())
      .pipe(gulpBabel({
        presets: ['es2015']
      }))
      .pipe(uglify({
        mangle: false
      }))
      .pipe(gulp.dest(path.build.js));
});

//Vendor JS build=======================================>
gulp.task('jsvendor:build', function () {
  gulp.src(path.src.jsvendor)
      .pipe(rigger())
      .pipe(uglify({
        mangle: false
      }))
      .pipe(gulp.dest(path.build.js));
});

//Images build==========================================>
gulp.task('image:build', function () {
  gulp.src(path.src.img)
  //.pipe(image())
  // .pipe(imagemin({
  // 	progressive: true,
  // 	svgoPlugins: [{removeViewBox: false}],
  // 	use: [pngquant()]
  // }))
      .pipe(gulp.dest(path.build.img));
});

//SVG sprite build=====================================>
gulp.task('sprite-svg:build', function () {
  return gulp.src(path.src.spritesvg)
      .pipe(spritesvg({
        'mode': {
          'symbol': {
            'dimentions': true,
            'dest': 'img',
            'sprite': '../img/sprite-inline.svg',
            'bust': false,
            example: true
          }
        }
      }))
      .pipe(gulp.dest('build'));
});

//Fonts build========================================>
gulp.task('fonts:build', function () {
  gulp.src(path.src.fonts)
      .pipe(gulp.dest(path.build.fonts));
});

gulp.task('root:build', function () {
  gulp.src(path.src.root)
      .pipe(gulp.dest(path.build.root));
});

//Devaring build folder=============================>
gulp.task('clean', function (cb) {
  rimraf(path.clean, cb);
});

//Tasks starting===================================>
//Files watching
gulp.task('watch', function () {
  watch([path.watch.html], function () {
    gulp.start('html:build');
  });
  watch([path.watch.sass], function () {
    gulp.start('sass:build');
  });
  watch([path.watch.sassvendor, './src/less/sass.less'], function () {
    gulp.start('sass:build');
    gulp.start('sassvendor:build');
  });
  watch([path.watch.js], function () {
    gulp.start('js:build');
  });
  watch([path.watch.jsvendor, path.src.jsvendor], function () {
    gulp.start('jsvendor:build');
    gulp.start('js:build');
  });
  watch([path.watch.img], function () {
    gulp.start('image:build');
  });
  watch(['./src/img/sprite/svg/*.svg'], function () {
    gulp.start('sprite-svg:build');
    gulp.start('html:build');
  });
  watch([path.watch.root], function () {
    gulp.start('root:build');
  });
});

//Building==================================================>
gulp.task('build', [
  'html:build',
  'sass:build',
  'fonts:build',
  'sassvendor:build',
  'image:build',
  'sprite-svg:build',
  'js:build',
  'jsvendor:build',
  'root:build'
]);

gulp.task('default', ['build', 'watch']);